@extends('layouts.admin')
@section('css')
<link rel="stylesheet" href="{{asset('assets/vendor/select2/select2.min.css')}}">
@endsection
@section('js')
<script src="{{asset('assets/vendor/select2/select2.min.js')}}"></script>
<script src="{{ asset('assets/tinymce/js/tinymce/tinymce.js') }}"></script>
<script type="text/javascript">
    tinymce.init({
  selector: 'textarea',
  height: 300,
  theme: 'modern',
  plugins: 'print preview fullpage  searchreplace autolink directionality  visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools  contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

</script>
<script type="text/javascript">
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('#lfm').filemanager('image');
    $('#lfm').filemanager('file');

</script>
@endsection

@section('header')
<nav class="breadcrumb-wrapper" aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/admin')}}"><i class="icon dripicons-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.artikel.index')}}">Artikel</a></li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">Edit Artikel</a></li>
    </ol>
</nav>
@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <h5 class="card-header">Tambah Artikel</h5>
        <form action="{{ route('admin.artikel.update',$artikel->id) }}" method="post" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label>Judul Artikel</label>
                    <input type="text" value="{{$artikel->judul}}" name="judul" class="form-control" placeholder="Masukan Judul" required>
                </div> 
                <div class="form-group">
                    <label>Konten</label>
                    <textarea name="konten">{!! $artikel->konten !!}</textarea>
                </div> 
                <div class="form-group">
                    <label>Kategori</label>
                     <select class="form-control" name="kategori_id" id="" required>
                        @foreach($kategori as $data)
                        <option value="{{ $data->id }}"{{ $selectedCategory == $data->id ? 'selected="selected"' : '' }}>{{ $data->nama_kategori }}</option>
                        @endforeach()
                    </select>
                </div>
                <div class="form-group">
                    <label>Tag</label>
                     <select class="form-control js-example-basic-multiple" name="tag[]" id="" required multiple="multiple">
                        @foreach($tag as $data)
                        <option value="{{ $data->id }}"{{ (in_array($data->id, $selected)) ? ' selected="selected"' : '' }}>{{ $data->name }}</option>
                        @endforeach()
                    </select>
                </div>
                @if (isset($artikel) && $artikel->foto)
                    {{--  <img  style="width:250px; height:250px;" alt="">  --}}
                    <img src="{{ $artikel->foto }}" id="holder" style="margin-top:15px;margin-bottom:15px;max-height:100px;">
            
                @endif
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary btn-outline">
                        <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="foto" value="{{$artikel->foto}}">
                </div>
                {{--  <div class="form-group">
                    <label>Foto</label>
                    @if (isset($artikel) && $artikel->foto)
                        <p>
                        <br>
                        <img src="{{ asset('assets/img/fotoartikel/'.$artikel->foto) }}" style="width:250px; height:250px;" alt="">
                        </p>
                    @endif
                    <input name="foto" type="file" value="{{$artikel->foto}}">
                </div>     --}}
            </div>
            <div class="card-footer bg-light">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="button" class="btn btn-secondary clear-form">Clear</button>
            </div>
        </form>
    </div>
</div>
@endsection