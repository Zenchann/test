<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Assalaam | Sign In</title>
	<!-- ================== GOOGLE FONTS ==================-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
	<!-- ======================= GLOBAL VENDOR STYLES ========================-->
	<link rel="stylesheet" href="../assets/css/vendor/bootstrap.css">
	<link rel="stylesheet" href="../assets/vendor/metismenu/dist/metisMenu.css">
	<link rel="stylesheet" href="../assets/vendor/switchery-npm/index.css">
	<link rel="stylesheet" href="../assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
	<!-- ======================= LINE AWESOME ICONS ===========================-->
	<link rel="stylesheet" href="../assets/css/icons/line-awesome.min.css">
	<!-- ======================= DRIP ICONS ===================================-->
	<link rel="stylesheet" href="../assets/css/icons/dripicons.min.css">
	<!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
	<link rel="stylesheet" href="../assets/css/icons/material-design-iconic-font.min.css">
	<!-- ======================= GLOBAL COMMON STYLES ============================-->
	<link rel="stylesheet" href="../assets/css/common/main.bundle.css">
	<!-- ======================= LAYOUT TYPE ===========================-->
	<link rel="stylesheet" href="../assets/css/layouts/vertical/core/main.css">
	<!-- ======================= MENU TYPE ===========================================-->
	<link rel="stylesheet" href="../assets/css/layouts/vertical/menu-type/default.css">
	<!-- ======================= THEME COLOR STYLES ===========================-->
	<link rel="stylesheet" href="../assets/css/layouts/vertical/themes/theme-a.css">
	<link rel="icon" type="image/png" href="{{ asset('assets/img/ico.png') }}" />x
</head>

<body>
	<div class="container">
        <form class="sign-in-form" action="{{ route('login') }}" method="post">
            @csrf
			<div class="card">
				<div class="card-body">
					<a href="index-2.html" class="brand text-center d-block m-b-20">
						<img src="../assets/img/ico.png" style="height:100px; width:100px;" alt="QuantumPro Logo" />
					</a>
					<h5 class="sign-in-heading text-center m-b-20">Sign in to your account</h5>
					<div class="form-group">
						<label for="inputEmail" class="sr-only">Email address</label>
						<input type="email" name="email" id="inputEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" required="">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

					<div class="form-group">
						<label for="inputPassword" class="sr-only">Password</label>
						<input type="password" name="password" id="inputPassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required="">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
					<div class="checkbox m-b-10 m-t-20">
						<div class="custom-control custom-checkbox checkbox-primary form-check">
							<input type="checkbox" class="custom-control-input" id="stateCheck1"{{ old('remember') ? 'checked' : '' }}>
							<label class="custom-control-label" for="stateCheck1">	Remember me</label>
						</div>
						<a href="{{ route('password.request') }}" class="float-right">Forgot Password?</a>
					</div>
					<button class="btn btn-primary btn-rounded btn-floating btn-lg btn-block" type="submit">Sign In</button>
				 <p class="text-muted m-t-25 m-b-0 p-0">Don't have an account yet?<a href="{{ route('register') }}"> Create an account</a></p>
				</div>

			</div>
		</form>
	</div>

	<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
	<script src="../assets/vendor/modernizr/modernizr.custom.js"></script>
	<script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
	<script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<script src="../assets/vendor/js-storage/js.storage.js"></script>
	<script src="../assets/vendor/js-cookie/src/js.cookie.js"></script>
	<script src="../assets/vendor/pace/pace.js"></script>
	<script src="../assets/vendor/metismenu/dist/metisMenu.js"></script>
	<script src="../assets/vendor/switchery-npm/index.js"></script>
	<script src="../assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<!-- ================== GLOBAL APP SCRIPTS ==================-->
	<script src="../assets/js/global/app.js"></script>

</body>
</html>
