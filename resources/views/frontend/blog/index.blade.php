@extends('layouts.frontend')
@section('content')
<section class="banner-area relative" id="home">	
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Blog Home				
                </h1>	
                <p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="blog-home.html"> Blog Home</a></p>
            </div>											
        </div>
    </div>
</section>
<section class="blog-posts-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 post-list blog-post-list">
                @foreach($artikel as $data)
                @if($data->status == 0)
                @else
                    <div class="single-post">
                        <img class="img-fluid" src="{{ asset($data->foto) }}" alt="">
                        <ul class="tags">
                            @foreach($data->Tag as $tag)
                                <li><a href="/blog/tag/{{ $tag->slug }}">#{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                        <a href="/blog/{{ $data->slug }}">
                            <h1>
                                {{ $data->judul }}
                            </h1>
                        </a>
                            <p>
                                {!! str_limit($data->konten,500) !!}
                            </p>
                        <div class="bottom-meta">
                            <div class="user-details row align-items-center">
                                <div class="comment-wrap col-lg-6">
                                    <ul>
                                        <li><a href="#"><span class="lnr lnr-heart"></span>	4 likes</a></li>
                                        <li><a href="#"><span class="lnr lnr-bubble"></span> 06 Comments</a></li>
                                    </ul>
                                </div>
                                <div class="social-wrap col-lg-6">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                    </ul>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                {{ $artikel->links() }}
    																					
            </div>
            @include('frontend.blog.side')
        </div>
    </div>	
</section>

@endsection
