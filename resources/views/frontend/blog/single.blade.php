@extends('layouts.frontend')
@section('content')

<section class="banner-area relative" id="home">    
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Blog Single               
                </h1>   
                <p class="text-white link-nav"><a href="{{ url('/') }}">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="blog-single.html"> Blog Single</a></p>
            </div>                                          
        </div>
    </div>
</section>
<section class="blog-posts-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 post-list blog-post-list">
                <div class="single-post">
                    <img class="img-fluid" src="{{ asset($artikel->foto) }}" alt="">
                    <ul class="tags">
                        @foreach($artikel->Tag as $key)
                        <li><a href="/blog/tag/{{ $key->slug}}">{{ $key->name}}</a></li>
                        @endforeach
                    </ul>
                    <a href="#">
                        <h1>
                            {{ $artikel->judul }}
                        </h1>
                    </a>
                    <div class="content-wrap">
                        <p>
                            {!! $artikel->konten !!}
                        </p>

                    </div>
                    <div class="bottom-meta">
                        <div class="user-details row align-items-center">
                            <div class="comment-wrap col-lg-6 col-sm-6">
                                <ul>
                                    <li><a href="#"><span class="lnr lnr-heart"></span>	4 likes</a></li>
                                    <li><a href="#"><span class="lnr lnr-bubble"></span> 06 Comments</a></li>
                                    <li><a href="/blog/kategori/{{ $artikel->Kategori->nama_kategori }}"><span class="lnr lnr-bubble"></span> {{ $artikel->Kategori->nama_kategori }}</a></li>
                                </ul>
                            </div>
                            <div class="social-wrap col-lg-6">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                </ul>
                                
                            </div>
                        </div>
                    </div>

                <!-- Start nav Area -->
                <section class="nav-area pt-50 pb-100">
                    <div class="container">
                        <div class="row justify-content-between">
                            @if(isset($previous))
                            <div class="col-sm-6 nav-left justify-content-start d-flex">
                                <div class="thumb">
                                    <img src="/frontend/img/blog/prev.jpg" alt="">
                                </div>
                                <div class="post-details">
                                    <p>{{ $previous->judul }}</p>
                                    <h4 class="text-uppercase"><a href="{{ URL::to( 'blog/' . $previous->slug) }}">Previous</a></h4>
                                </div>
                            </div>
                            @endif
                            @if(isset($next))
                            <div class="col-sm-6 nav-right justify-content-end d-flex" style="flex: 0 0 100%; max-width:50%;">
                                <div class="post-details">
                                    <p>{{ $next->judul }}</p>
                                    <h4 class="text-uppercase"><a href="{{ URL::to( 'blog/' . $next->slug) }}">Next</a></h4>
                                </div>
                                             
                                <div class="thumb">
                                    <img src="/frontend/img/blog/next.jpg" alt="">
                                </div>                         
                            </div>
                            @endif
                        </div>
                    </div>    
                </section>
                <!-- End nav Area -->

                <div id="disqus_thread"></div>
                </div>																		
            </div>
            {{--  view composer  --}}
            @include('frontend.blog.side')
            {{--  end view composer  --}}
        </div>
    </div>	
</section>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://prakerin-1.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            
@endsection
