<header id="header" id="home" class="header-scrolled">
  <div class="container main-menu">
    <div class="row align-items-center justify-content-between d-flex">
      <div id="logo">
        <a href="index.html"><img src="{{ asset('frontend/img/logo.png')}}" alt="" title="" /></a>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="{{ url('/') }}">Home</a></li>
          <li><a href="{{ url('about') }}">About Us</a></li>
          <li><a href="{{ url('service') }}">Services</a></li>
          <li><a href="{{ url('product') }}">Products</a></li>
          <li class="menu-has-children"><a href="{{ url('blog') }}">Blog</a>
          </li>						          
          <li><a href="{{ url('contact') }}">Contact</a></li>				              
        </ul>
      </nav><!-- #nav-menu-container -->		    		
    </div>
  </div>
</header>