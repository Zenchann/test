<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(
    ['prefix'=>'admin', 'as' => 'admin.', 'middleware'=>['auth','role:admin|member']],
function () {
    Route::get('/', 'AdminController@index');
    Route::resource('kategori', 'KategoriController');
    Route::resource('tag', 'TagController');
    Route::resource('artikel', 'ArtikelController');
    Route::post('artikel/{publish}', 'ArtikelController@Publish')->name('artikel.publish');
}
);

// frontend
Route::get('blog', 'FrontendController@blog');
Route::get('about', 'FrontendController@about');
Route::get('service', 'FrontendController@service');
Route::get('product', 'FrontendController@product');
Route::get('contact', 'FrontendController@contact');
Route::get('blog/{artikel}', 'FrontendController@singleblog');
Route::get('blog/kategori/{kategori}', 'FrontendController@artikelkategori');
Route::get('blog/tag/{tag}', 'FrontendController@artikeltag');

// cekson
Route::get('instagram', 'FrontendController@cekinsta');

// Api
Route::resource('/kontak', 'KontakController');
