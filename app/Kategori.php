<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Kategori extends Model
{
    protected $fillable = ['nama_kategori','slug'];
    public $timestamps = true;

    public function Artikel()
    {
        return $this->hasMany('App\Artikel', 'kategori_id');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($kategori) {
            // mengecek apakah penulis masih punya buku
            if ($kategori->Artikel->count() > 0) {
                // menyiapkan pesan error
                $html = 'Kategori tidak bisa dihapus karena masih digunakan oleh Artikel : ';
                $html .= '<ul>';
                foreach ($kategori->Artikel as $data) {
                    $html .= "<li>$data->judul</li>";
                }
                $html .= '</ul>';
                Session::flash("flash_notification", [
                "level"=>"danger",
                "message"=>$html
                ]);
                // membatalkan proses penghapusan
                return false;
            }
        });
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
