<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel;
use App\Kategori;
use Vinkla\Instagram\Instagram;
use App\Tag;

class FrontendController extends Controller
{
    public function index()
    {
        $artikel = Artikel::orderBy('created_at', 'desc')->take(4)->get();
        return view('index', compact('artikel'));
    }
   
    public function blog()
    {
        $artikel = Artikel::orderBy('created_at', 'desc')->paginate(5);
        return view('frontend.blog.index', compact('artikel'));
    }

    public function artikelkategori(Kategori $kategori)
    {
        $artikel = $kategori->Artikel()->latest()->paginate(5);
        return view('frontend.blog.index', compact('artikel'));
    }
    public function artikeltag(Tag $tag)
    {
        $artikel = $tag->Artikel()->latest()->paginate(5);
        return view('frontend.blog.index', compact('artikel'));
    }

    public function singleblog(Artikel $artikel)
    {
        $previous = Artikel::where('id', '<', $artikel->id)->orderBy('id', 'desc')->first();
        $next = Artikel::where('id', '>', $artikel->id)->orderBy('id')->first();
        // dd($next);
        return view('frontend.blog.single', compact('artikel', 'next', 'previous'));
    }
    public function about()
    {
        return view('frontend.about');
    }
    public function service()
    {
        return view('frontend.service');
    }
    public function product()
    {
        return view('frontend.product');
    }
    public function contact()
    {
        return view('frontend.contact');
    }

    public function cekinsta()
    {
        $instagram = new Instagram('1722842458.1677ed0.4a12518cab3f446eaf28cb917d8fed41');
        dd($instagram);
    }
}
